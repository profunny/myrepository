package belhard.task10_butterfly;

import java.util.Scanner;

/**
 * Created by andrei on 30.04.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = scanner.nextInt();
        int array[][] = new int[size][size];
        Butterfly butterfly = new Butterfly();

        butterfly.getUpperWing(array);
        butterfly.getBottomWing(array);

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
    }
}


//    Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите число: ");
//    int size = scanner.nextInt();
//    int array[][] = new int[size][size];
//    int counter = 0;
//
//        for (int i = 0; i < size; i++) {
//        for (int j = counter; j < size - counter; j++) {
//            array[i][j] = 1;
//        }
//        counter++;
//    }
//
//        for (int i = 0; i < size; i++) {
//        counter--;
//        for (int j = counter; j < size - counter; j++) {
//            array[i][j] = 1;
//        }
//    }
//
//        for (int i = 0; i < size; i++) {
//        for (int j = 0; j < size; j++) {
//            System.out.print(array[i][j]);
//        }
//        System.out.println();
//    }
//}
//}




