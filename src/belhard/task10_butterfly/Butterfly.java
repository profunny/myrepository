package belhard.task10_butterfly;

/**
 * Created by andrei on 01.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Butterfly {
    private int counter = 0;

    /**
     * This method draws the upper wing.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @return array after replacing zero values.
     */
    int getUpperWing(int array[][]) {

        for (int i = 0; i < array.length; i++) {
            for (int j = counter; j < array.length - counter; j++) {
                array[i][j] = 1;
            }
            counter++;
        }
        return array[array.length - 1][array.length - 1];
    }

    /**
     * This method draws the bottom wing.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @return array after replacing zero values.
     */
    int getBottomWing(int array[][]) {

        for (int i = 0; i < array.length; i++) {
            counter--;
            for (int j = counter; j < array.length - counter; j++) {
                array[i][j] = 1;
            }
        }
        return array[array.length - 1][array.length - 1];
    }
}
