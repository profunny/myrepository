package belhard.task4_calculator;

import java.util.Scanner;

/**
 * Created by andrei on 19.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("Введите 2 числа: ");
        Scanner scanner = new Scanner(System.in);
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();

        System.out.println("Выберите действие: ");
        System.out.println("1 - сложение");
        System.out.println("2 - вычитание");
        System.out.println("3 - умножение");
        System.out.println("4 - деление");

        int action = scanner.nextInt();
        Operation operation = new Operation();
        try {

            switch (action) {
                case 1: {
                    int result = operation.amount(number1, number2);
                    System.out.println("Сумма чисел равна: " + result);
                }
                break;
                case 2: {
                    int result = operation.difference(number1, number2);
                    System.out.println("Разность чисел равна: " + result);
                }
                break;
                case 3: {
                    int result = operation.composition(number1, number2);
                    System.out.println("Произведение чисел равно: " + result);
                }
                break;
                case 4: {
                    try {
                        String result = operation.division(number1, number2);
                        System.out.println("Частное чисел равно: " + result);
                    } catch (ZeroException e) {
                        System.err.println(e.getMessage());
                    }
                }
                break;
            }
        } catch (OutOfRangeException e) {
            System.err.println(e.getMessage());
        }
    }
}
