package belhard.task4_calculator;

/**
 * Created by andrei on 26.04.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
class OutOfRangeException extends Exception {
    OutOfRangeException(String message) {
        super(message);
    }
}
