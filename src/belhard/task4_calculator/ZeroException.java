package belhard.task4_calculator;

/**
 * Created by andrei on 19.04.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class ZeroException extends Exception {
    public ZeroException(String message) {
        super(message);
    }
}
