package belhard.task4_calculator;

/**
 * Created by andrei on 19.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
class Operation {

    /**
     * This method returns the amount of the entered numbers.
     *
     * @param firstNumber  an integer contains the entered number by the user.
     * @param secondNumber an integer contains the entered number by the user.
     * @return the sum of entered numbers.
     * @throws OutOfRangeException processes the exception at the output from the range.
     */
    int amount(int firstNumber, int secondNumber) throws OutOfRangeException {

        if (firstNumber + secondNumber > 2_147_483_647 || firstNumber + secondNumber < -2_147_483_648) {
            throw new OutOfRangeException("Ошибка: выход за предел диапозона.");
        }
        return firstNumber + secondNumber;
    }

    /**
     * This method returns the difference of the entered numbers.
     *
     * @param firstNumber  an integer contains the entered number by the user.
     * @param secondNumber an integer contains the entered number by the user.
     * @return the difference of entered numbers.
     * @throws OutOfRangeException processes the exception at the output from the range.
     */
    int difference(int firstNumber, int secondNumber) throws OutOfRangeException {

        if (firstNumber - secondNumber > 2_147_483_647 || firstNumber - secondNumber < -2_147_483_648) {
            throw new OutOfRangeException("Ошибка: выход за предел диапозона.");
        }
        return firstNumber - secondNumber;
    }

    /**
     * This method returns the composition of the entered numbers.
     *
     * @param firstNumber  an integer contains the entered number by the user.
     * @param secondNumber an integer contains the entered number by the user.
     * @return the composition of entered numbers.
     * @throws OutOfRangeException processes the exception at the output from the range.
     */
    int composition(int firstNumber, int secondNumber) throws OutOfRangeException {

        if (firstNumber * secondNumber > 2_147_483_647 || firstNumber * secondNumber < -2_147_483_648) {
            throw new OutOfRangeException("Ошибка: выход за предел диапозона.");
        }
        return firstNumber * secondNumber;
    }

    /**
     * This method returns the division of the entered numbers.
     *
     * @param firstNumber  an double contains the entered number by the user.
     * @param secondNumber an double contains the entered number by the user.
     * @return the division of the entered numbers.
     * @throws ZeroException       processes the exception then dividing by 0.
     * @throws OutOfRangeException processes the exception at the output from the range.
     */
    String division(double firstNumber, double secondNumber) throws ZeroException, OutOfRangeException {

        if (secondNumber == 0) {
            throw new ZeroException("Ошибка: деление на 0.");
        }
        if (firstNumber / secondNumber > 2_147_483_647 || firstNumber / secondNumber < -2_147_483_648) {
            throw new OutOfRangeException("Ошибка: выход за предел диапозона.");
        }

        double result = firstNumber / secondNumber;
        String formattedResult = String.format("%.3f", result);
        return formattedResult;
    }
}
