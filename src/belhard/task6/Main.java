package belhard.task6;

import java.util.Scanner;

/**
 * Created by andrei on 20.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите значение x: ");
        int number1 = scanner.nextInt();
        System.out.println("Введите значение y: ");
        int number2 = scanner.nextInt();

        Changer changerOne = new Changer(number1, number2);
        changerOne.changeValue(changerOne);
        System.out.println("Поменять значения (используя дополнительную переменную):");
        System.out.println("x = " + changerOne.number1);
        System.out.println("y = " + changerOne.number2);

        Changer changerTwo = new Changer(number1, number2);
        changerTwo.changeValueXor(changerTwo);
        System.out.println("Обратно сменить значения (используя Xor): ");
        System.out.println("x = " + changerTwo.number1);
        System.out.println("y = " + changerTwo.number2);
    }
}
