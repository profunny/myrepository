package belhard.task6;

/**
 * Created by andrei on 20.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
class Changer {

    int number1;
    int number2;

    Changer(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    /**
     * This method changes the values in places with additional variable.
     *
     * @param changerOne includes the variables number1 and number2.
     * @return object changerOne with changed variables.
     */
    Changer changeValue(Changer changerOne) {

        int number3 = number2;
        number2 = number1;
        number1 = number3;

        return changerOne;
    }

    /**
     * This method changes the values in places with operation xor.
     *
     * @param changerTwo includes the variables number1 and number2.
     * @return object changerTwo with changed variables.
     */
    Changer changeValueXor(Changer changerTwo) {

        number1 = number1 ^ number2;
        number2 = number1 ^ number2;
        number1 = number1 ^ number2;

        return changerTwo;
    }
}
