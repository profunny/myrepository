package belhard.task5_NODandNOK;


import java.util.Scanner;

/**
 * Created by andrei on 20.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Введите 2 числа: ");
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        NOD nod = new NOD();
        NOK nok = new NOK();

        int resultNOD = nod.calculateNOD(a, b);
        int resultNOK = nok.calculateNOK(a, b);

        System.out.println("НОД чисел: " + resultNOD);
        System.out.println("НОК чисел: " + resultNOK);
    }
}
