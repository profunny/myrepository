package belhard.task5_NODandNOK;

/**
 * Created by andrei on 20.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
class NOK {

    /**
     * This method calculate the least common multiple of the entered numbers.
     *
     * @param a an integer contains the entered number by the user.
     * @param b an integer contains the entered number by the user.
     * @return the least common multiple of the entered numbers.
     */

    public int calculateNOK(int a, int b) {
        NOD nod = new NOD();
        int resultNOD = nod.calculateNOD(a, b);
        int resultNOK = a * b / resultNOD;
        return resultNOK;
    }
}
