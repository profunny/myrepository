package belhard.task5_NODandNOK;

/**
 * Created by andrei on 20.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
class NOD {

    /**
     * This method calculate the greatest common divisor of the entered numbers.
     *
     * @param a an integer contains the entered number by the user.
     * @param b an integer contains the entered number by the user.
     * @return the greatest common divisor of the entered numbers.
     */
    int calculateNOD(int a, int b) {
        int n = 2;
        int resultNOD = 1;
        if (a <= b) {
            a = a ^ b;
            b = a ^ b;
            a = a ^ b;
        }
        for (; ; ) {
            if (n > b) {
                break;
            }
            int remainder1 = a % n;
            int remainder2 = b % n;
            if (remainder1 == 0 && remainder2 == 0) {
                a = a / n;
                b = b / n;
                resultNOD = resultNOD * n;
            } else {
                n++;
            }
        }
        return resultNOD;
    }
}
