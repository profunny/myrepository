package belhard.task1_abs;

/**
 * Created by andrei on 26.04.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
class MinIntException extends Exception {

    MinIntException(String message) {
        super(message);
    }
}
