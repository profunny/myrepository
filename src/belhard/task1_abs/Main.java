package belhard.task1_abs;


import java.util.Scanner;

/**
 * Created by andrei on 19.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("Введите число: ");

        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        Operation operation = new Operation();

        try {
            int result = operation.absNumVer1(number);
            System.out.println("Модуль введенного числа: " + result);
        } catch (MinIntException e) {
            System.err.println(e.getMessage());
        }

        try {
            int result = operation.absNumVer2(number);
            System.out.println("Модуль введенного числа: " + result);
        } catch (MinIntException e) {
            System.err.println(e.getMessage());
        }
    }
}
