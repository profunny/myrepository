package belhard.task1_abs;

/**
 * Created by andrei on 19.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
class Operation {

    /**
     * This method calculates the module of the entered number with class {@link Math}.
     *
     * @param number an integer contains the entered number by the user.
     * @return the module of the entered number.
     * @throws MinIntException processes the exception resulting from the input the number -2_147_483_648.
     */

    int absNumVer1(int number) throws MinIntException {

        if (number == -2_147_483_648) {
            throw new MinIntException("Ошибка: выход за предел диапозона.");
        }
        return Math.abs(number);
    }

    /**
     * This method calculates the module of the entered number.
     *
     * @param number an integer contains the entered number by the user.
     * @return the module of the entered number.
     * @throws MinIntException processes the exception resulting from the input the number -2_147_483_648.
     */
    int absNumVer2(int number) throws MinIntException {

        if (number == -2_147_483_648) {
            throw new MinIntException("Ошибка: выход за предел диапозона.");
        }
        if (number < 0) {
            return number * (-1);
        } else {
            return number;
        }
    }
}
