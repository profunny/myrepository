package belhard.task11_snake;

/**
 * Created by andrei on 01.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Snake {

    private int iCounter = 0;
    private int jCounter = 0;

    /**
     * This method determines the direction of the snake.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @param a     an integer sets the height of the matrix "array".
     * @param b     an integer sets the width of the matrix "array".
     * @return array after replacing zero values.
     */
    int getAllSnake(int array[][], int a, int b) {
        for (; ; ) {
            getRight(array, b);
            if (jCounter < (b + 1) / 2) {
                break;
            }
            getDown(array, a);
            if (iCounter < (a + 1) / 2) {
                break;
            }
            getLeft(array);
            if (jCounter > (b - 1) / 2) {
                break;
            }
            getUp(array);
            if (iCounter > (a - 1) / 2) {
                break;
            }
        }
        return array[array.length - 1][array.length - 1];
    }

    /**
     * This method draws the snake in the right direction.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @param b     an integer sets the width of the matrix "array".
     * @return array after replacing zero values.
     */
    private int getRight(int array[][], int b) {
        for (int j = jCounter; j < b - 1; j++) {
            int i = iCounter;
            jCounter++;
            if (array[iCounter][jCounter] == 1) {
                array[i][j] = 0;
                jCounter--;
                jCounter--;
                break;
            } else if (array[iCounter][jCounter] == 0) {
                array[i][j] = 1;
            }

        }
        return array[array.length - 1][array.length - 1];
    }

    /**
     * This method draws the snake in the down direction.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @param a     an integer sets the height of the matrix "array".
     * @return array after replacing zero values.
     */
    private int getDown(int array[][], int a) {
        for (int i = iCounter; i < a - 1 ; i++) {
            int j = jCounter;
            iCounter++;
            if (array[iCounter][jCounter] == 1) {
                array[i][j] = 0;
                iCounter--;
                iCounter--;
                break;
            } else if (array[iCounter][jCounter] == 0) {
                array[i][j] = 1;
            }
        }
        return array[array.length - 1][array.length - 1];
    }

    /**
     * This method draws the snake in the left direction.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @return array after replacing zero values.
     */
    private int getLeft(int array[][]) {
        for (int j = jCounter; j > 0; j--) {
            int i = iCounter;
            jCounter--;
            if (array[iCounter][jCounter] == 1) {
                array[i][j] = 0;
                jCounter++;
                jCounter++;
                break;
            } else if (array[iCounter][jCounter] == 0) {
                array[i][j] = 1;
            }
        }
        return array[array.length - 1][array.length - 1];
    }

    /**
     * This method draws the snake in the up direction.
     *
     * @param array an integer double matrix that contains "0" in all cells as default.
     * @return array after replacing zero values.
     */
    private int getUp(int array[][]) {
        for (int i = iCounter; i > 0; i--) {
            int j = jCounter;
            iCounter--;
            if (array[iCounter][jCounter] == 1) {
                array[i][j] = 0;
                iCounter++;
                iCounter++;
                break;
            } else if (array[iCounter][jCounter] == 0) {
                array[i][j] = 1;
            }
        }
        return array[array.length - 1][array.length - 1];
    }
}
