package belhard.task11_snake;

import java.util.Scanner;

/**
 * Created by andrei on 30.04.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива (a и b): ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int array[][] = new int[a][b];

        Snake snake = new Snake();
        snake.getAllSnake(array, a, b);

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
    }
}








//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите число: ");
//        int a = scanner.nextInt();
//        int b = scanner.nextInt();
//        int array[][] = new int[a][b];
//        int iCounter = 0;
//        int jCounter = 0;
//
//        for (; ; ) {
//
//            for (int j = jCounter; j < b - 1; j++) {
//                int i = iCounter;
//                jCounter++;
//                if (array[iCounter][jCounter] == 1) {
//                    array[i][j] = 0;
//                    jCounter--;
//                    jCounter--;
//                    break;
//                } else if (array[iCounter][jCounter] == 0) {
//                    array[i][j] = 1;
//                }
//            }
//
//            if (jCounter < (b + 1) / 2) {
//                break;
//            }
//
//            for (int i = iCounter; i < a - 1; i++) {
//                int j = jCounter;
//                iCounter++;
//                if (array[iCounter][jCounter] == 1) {
//                    array[i][j] = 0;
//                    iCounter--;
//                    iCounter--;
//                    break;
//                } else if (array[iCounter][jCounter] == 0) {
//                    array[i][j] = 1;
//                }
//            }
//
//            if (iCounter < (a + 1) / 2) {
//                break;
//            }
//
//            for (int j = jCounter; j > 0; j--) {
//                int i = iCounter;
//                jCounter--;
//                if (array[iCounter][jCounter] == 1) {
//                    array[i][j] = 0;
//                    jCounter++;
//                    jCounter++;
//                    break;
//                } else if (array[iCounter][jCounter] == 0) {
//                    array[i][j] = 1;
//                }
//            }
//
//            if (jCounter > (b - 1) / 2) {
//                break;
//            }
//
//            for (int i = iCounter; i > 0; i--) {
//                int j = jCounter;
//                iCounter--;
//                if (array[iCounter][jCounter] == 1) {
//                    array[i][j] = 0;
//                    iCounter++;
//                    iCounter++;
//                    break;
//                } else if (array[iCounter][jCounter] == 0) {
//                    array[i][j] = 1;
//                }
//            }
//
//            if (iCounter > (a - 1) / 2) {
//                break;
//            }
//        }
//
//        for (int i = 0; i < a; i++) {
//            for (int j = 0; j < b; j++) {
//                System.out.print(array[i][j]);
//            }
//            System.out.println();
//        }
//    }
//}
