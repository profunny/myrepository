package belhard.task12;

/**
 * Created by andrei on 02.05.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Polindrom {

    /**
     * This method removes all characters except numeric, alphabetic symbols and spaces.
     *
     * @param line as String contain the text line.
     * @return the line without all characters except numeric, alphabetic symbols and spaces.
     */
    String deleteSigns(String line) {
        return line.replaceAll("[\\,,\\.,\\!,\\?,\\:,\\;]", "");
    }

    /**
     * This method divides the line into words.
     *
     * @param line as String contain the text line.
     * @return the matrix of the String objects.
     */
    String[] divideLine(String line) {
        return line.split("[ ]");
    }

    /**
     * This method compares each element of an array with reversing value.
     *
     * @param lineArray is array which contains the String elements.
     * @return result, which contains only the words-polindroms.
     */
    StringBuilder showPolindroms(String[] lineArray) {
        StringBuilder mirror = new StringBuilder();
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < lineArray.length; i++) {
            mirror.append(lineArray[i]);
            mirror.reverse();

            if (lineArray[i].equals(mirror.toString())) {
                result.append(lineArray[i] + " ");
            }
            mirror.delete(0, mirror.length());
        }
        return result;
    }
}

//        System.out.println(TEXT.replaceAll("[Тт]а[ий]ланд", "Украина"));
