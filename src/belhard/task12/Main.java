package belhard.task12;

import java.util.Scanner;

/**
 * Created by andrei on 01.05.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Введите строку: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        Polindrom polindrom = new Polindrom();

        line = polindrom.deleteSigns(line);
        String[] lineArray = polindrom.divideLine(line);

        System.out.println("Слова полиндромы: " + polindrom.showPolindroms(lineArray));
    }
}
