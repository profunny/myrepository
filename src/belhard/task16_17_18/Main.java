package belhard.task16_17_18;

/**
 * Created by andrei on 08.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Main {

    public static void main(String[] args) {

        Cat[] cats = new Cat[10];
        cats[0] = new Cat("murzik", 5);
        cats[1] = new Cat("vasya", 3);
        cats[2] = new Cat("kesha", 8);
        cats[3] = new Cat("vlastelin", 2);
        cats[4] = new Cat("baskov", 7);
        cats[5] = new Cat("maryana", 5);
        cats[6] = new Cat("lucifer", 66);
        cats[7] = new Cat("pavlik", 6);
        cats[8] = new Cat("murzik", 4);
        cats[9] = new Cat("vasilisa", 3);

        CatUtils.sortCatByName(cats);

        for (int i = 0; i < cats.length; i++) {
            System.out.println(cats[i].toString());
        }
    }
}
