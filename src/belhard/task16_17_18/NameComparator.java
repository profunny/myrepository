package belhard.task16_17_18;

import java.util.Comparator;

/**
 * Created by andrei on 09.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class NameComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        if (o2 == null || !o2.getClass().equals(Cat.class))
            return 1;
        if (o1 == null || !o1.getClass().equals(Cat.class))
            return -1;

        Cat first = (Cat) o1;
        Cat second = (Cat) o2;

        return first.getName().compareTo(second.getName());
    }
}
