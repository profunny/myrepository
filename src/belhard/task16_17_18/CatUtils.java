package belhard.task16_17_18;

import java.util.Arrays;

/**
 * Created by andrei on 10.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class CatUtils {
    public static void sortCatByName(Cat[] cats) {
        Arrays.sort(cats, new NameComparator());
    }
}
