package belhard.task16_17_18;

/**
 * Created by andrei on 08.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Zoo {

    String name;
    int age;

    public Zoo(Cat cat) {
        this.name = cat.getName();
        this.age = cat.getAge();
    }
}
