package belhard.task16_17_18;

/**
 * Created by andrei on 08.05.2017.
 *
 * @author andrei
 * @version 1.0
 * @since JDK 1.8
 */
public class Dog {

    private static Dog dog;

    private Dog() {

    }

    public static Dog getDog() {
        if (dog == null) {
            dog = new Dog();
        }
        return dog;
    }
}
