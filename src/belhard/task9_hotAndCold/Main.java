package belhard.task9_hotAndCold;

import java.util.Scanner;

/**
 * Created by andrei on 21.04.2017.
 *
 * @author andrei
 * @version 1.2
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("Введите диапозон значений. Нижняя граница диапозона:  ");
        Scanner scanner = new Scanner(System.in);
        int minRange = scanner.nextInt();
        System.out.println("Верхняя граница диапозона: ");
        int maxRange = scanner.nextInt();

        Operation operation = new Operation();
        int ranNumb = operation.generateRanNumb(maxRange, minRange);

        System.out.println("Введите число: ");
        operation.startPlay(ranNumb, maxRange, minRange);
        System.out.println(operation.message);

        for (;;) {
            if (operation.myNumb == ranNumb) {
                break;
            }
            operation.letsPlay(ranNumb, maxRange, minRange);
        }
        System.out.println("Количество попыток: " + operation.i);
    }
}
