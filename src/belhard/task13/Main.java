package belhard.task13;


import java.util.Scanner;

/**
 * Created by andrei on 30.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {
        int k = 0;

        System.out.println("Введите размер массива: ");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int[] array = new int[x];
        System.out.println("Введите значения: ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                k++;
            }
        }

        int[] positiveArray = new int[k];
        k = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                positiveArray[k] = array[i];
                k++;
            }
        }

        for (k = 0; k < positiveArray.length; k++) {
            System.out.print(positiveArray[k] + " ");
        }
        System.out.println();
    }
}

