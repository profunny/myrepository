package belhard.task15;


import java.util.Scanner;

/**
 * Created by andrei on 30.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Введите строку: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        System.out.println("Выберите действие: ");
        System.out.println("1 - удалить все числа из строки.");
        System.out.println("2 - заменить все символы русского алфавита символом ?. ");
        System.out.println("3 - оставить в строке только русские символы и числа.");
        int x = scanner.nextInt();

        WorkWithText action = new WorkWithText();

        switch (x) {
            case 1: {
                String result = action.deleteAllNumbers(line);
                System.out.println(result);
                break;
            }
            case 2: {
                String result = action.changeAllRussSymbols(line);
                System.out.println(result);
                break;
            }
            case 3: {
                String result = action.deleteAllNonRussAndNumbers(line);
                System.out.println(result);
                break;
            }
            default: {
                System.out.println(line);
            }
        }
    }
}
