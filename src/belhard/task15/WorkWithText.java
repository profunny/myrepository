package belhard.task15;


/**
 * Created by andrei on 30.04.2017.
 *
 * @author andrei
 * @version 1.1
 * @since JDK 1.8
 */
public class WorkWithText {

    /**
     * This method deletes all numbers from entering the String parameter.
     *
     * @param line a String and contains the entered symbols by the user.
     * @return all symbols from the String variable "line" except numbers.
     */
    String deleteAllNumbers(String line) {
        return line.replaceAll("[0-9]", "");
    }

    /**
     * This method changes all symbols of the Russian alphabet to sign "?".
     *
     * @param line a String and contains the entered symbols by the user.
     * @return all symbols from the String variable "line", but all symbols
     * from the Russian alphabet was changed to "?".
     */
    String changeAllRussSymbols(String line) {
        return line.replaceAll("[А-Я, а-я]", "?");
    }

    /**
     * This method deletes all symbols against numbers and symbols of the Russian alphabet.
     *
     * @param line a String and contains the entered symbols by the user.
     * @return all numbers and symbols of the Russian alphabet from
     * the String variable "line".
     */
    String deleteAllNonRussAndNumbers(String line) {
        return line.replaceAll("[^0-9,^А-Я,^а-я]", "");
    }
}
