package belhard.task21_22_23;

/**
 * Created by andrei on 08.05.2017.
 */
public class Man {
    String firstName;
    String secondName;
    int telNumber;


    public Man(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public Man(String firstName, int telNumber) {
        this.firstName = firstName;
        this.telNumber = telNumber;
    }

    public Man(String firstName, String secondName, int telNumber) {
        this(firstName,secondName);
        this.telNumber = telNumber;
    }

    @Override
    public String toString() {
        return "Man{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", telNumber=" + telNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Man man = (Man) o;

        return telNumber == man.telNumber;
    }
}




