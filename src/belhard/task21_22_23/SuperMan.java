package belhard.task21_22_23;

/**
 * Created by andrei on 08.05.2017.
 */
public class SuperMan extends Man {

    String superPower;

    public SuperMan(String firstName, String secondName, String superPower) {
        super(firstName, secondName);
        this.superPower = superPower;
    }

    public SuperMan(String firstName, int telNumber) {
        super(firstName, telNumber);
    }

    public SuperMan(String firstName, String secondName, int telNumber, String superPower) {
        super(firstName, secondName, telNumber);
        this.superPower = superPower;
    }

    @Override
    public String toString() {
        return "SuperMan{" +
                "firstName='" + firstName + '\'' +
                ", superPower='" + superPower + '\'' +
                ", secondName='" + secondName + '\'' +
                ", telNumber=" + telNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SuperMan superMan = (SuperMan) o;

        return superPower != null ? superPower.equals(superMan.superPower) : superMan.superPower == null;
    }
}
